﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logic.Dto.Patient;
using Newtonsoft.Json;

namespace WpfAssistantClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RegisterClick(object sender, RoutedEventArgs e)
        {
            var complaintText = new TextRange(Complaint.Document.ContentStart,Complaint.Document.ContentEnd).Text.Replace("\r\n","");
            long tajNumber;

            if (string.IsNullOrEmpty(Name.Text) || string.IsNullOrEmpty(Address.Text) || string.IsNullOrEmpty(TajNumber.Text) || string.IsNullOrEmpty(complaintText))
            {
                MessageBox.Show("Minden mező kitöltése kötelező!");
                return;
            }

            try
            {
                tajNumber = long.Parse(TajNumber.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("A tajszám nem megfelelő!");
                return;
            }

            using (var client = new HttpClient())
            {
                var patient = new PatientDto
                {
                    Name = Name.Text,
                    Address = Address.Text,
                    TajNumber = tajNumber,
                    Complaint = complaintText
                };

                var jsonPatient = JsonConvert.SerializeObject(patient);
                var content = new StringContent(jsonPatient, Encoding.UTF8, "application/json");

                try
                {
                    var result = client.PostAsync("https://localhost:44319/api/Patients", content).Result;

                    if (result.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Beteg Regisztrálva!");
                        Name.Text = string.Empty;
                        Address.Text = string.Empty;
                        TajNumber.Text = string.Empty;
                        Complaint.Document.Blocks.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Hiba a beteg regisztrálása közben!");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Hiba a beteg regisztrálása közben!\nA kapcsolat megszakadt a szerverrel!");
                }
            }
        }
    }
}
