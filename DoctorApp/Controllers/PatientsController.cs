﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.Dto;
using Logic.Dto.Patient;
using Logic.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DoctorApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientsController : ControllerBase
    {
        private readonly IPatientLogic _patientLogic;

        public PatientsController(IPatientLogic patientLogic)
        {
            _patientLogic = patientLogic;
        }

        [HttpPost(Name = nameof(AddPatient))]
        public ActionResult<PatientDto> AddPatient([FromBody] PatientCreateDto patientDto)
        {
            if (patientDto == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _patientLogic.Add(patientDto);
            
            return CreatedAtAction(nameof(AddPatient), patientDto);
        }

        [HttpGet]
        [Route("{id:int}", Name = nameof(GetPatient))]
        public ActionResult<PatientDto> GetPatient(int id)
        {
            var patientDto = _patientLogic.Get(id);
            if (patientDto == null)
            {
                return NotFound();
            }

            return Ok(patientDto);
        }

        [HttpGet(Name = nameof(GetPatients))]
        public ActionResult<List<PatientDto>> GetPatients()
        {
            var patientDtos = _patientLogic.Get().ToList();

            return Ok(patientDtos);
        }

        [HttpDelete]
        [Route("{id:int}", Name = nameof(DeletePatient))]
        public ActionResult DeletePatient(int id)
        {
            var patientDto = _patientLogic.Get(id);

            if (patientDto == null)
            {
                return NotFound();
            }

            _patientLogic.Delete(id);

            return NoContent();
        }
    }
}