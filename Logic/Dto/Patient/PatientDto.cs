﻿namespace Logic.Dto.Patient
{
    public class PatientDto : BaseDto
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public long TajNumber { get; set; }

        public string Complaint { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
