﻿namespace Logic.Dto.Patient
{
    public class PatientCreateDto
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public long TajNumber { get; set; }

        public string Complaint { get; set; }
    }
}
