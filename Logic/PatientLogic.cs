﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataAccess;
using Logic.Dto;
using Logic.Dto.Patient;
using Logic.Interface;
using Repository.Interface;

namespace Logic
{
    public class PatientLogic : IPatientLogic
    {
        private readonly IPatientRepository _patientRepository;

        public PatientLogic(IPatientRepository patientRepository)
        {
            _patientRepository = patientRepository;
        }

        public bool Add(PatientCreateDto patient)
        {
            return _patientRepository.Add(Mapper.Map<Patient>(patient));
        }

        public PatientDto Get(int id)
        {
            var patient = _patientRepository.Get(id);
            return Mapper.Map<PatientDto>(patient);
        }

        public IList<PatientDto> Get()
        {
            var patients = _patientRepository.Get().ToList();
            return Mapper.Map<List<PatientDto>>(patients);
        }

        public bool Delete(int id)
        {
            return _patientRepository.Delete(id);
        }
    }
}
