﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic.Dto;
using Logic.Dto.Patient;

namespace Logic.Interface
{
    public interface IPatientLogic
    {
        bool Add(PatientCreateDto patient);

        PatientDto Get(int id);

        IList<PatientDto> Get();

        bool Delete(int id);

    }
}
