﻿using DataAccess;
using Repository.Generic.Interface;

namespace Repository.Interface
{
    public interface IPatientRepository : IRepository<Patient>
    {
    }
}
