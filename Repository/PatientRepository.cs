﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using DataAccess;
using Repository.Generic;
using Repository.Interface;

namespace Repository
{
    public class PatientRepository : Repository<Patient>, IPatientRepository
    {
        public PatientRepository(ApiContext context) : base(context)
        {
        }

        public override Patient Get(int id)
        {
            return Get().FirstOrDefault(x => x.Id == id);
        }
    }
}
