﻿using System.Linq;

namespace Repository.Generic.Interface
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(int id);

        bool Add(TEntity entity);

        bool Delete(int id);

        bool Delete(TEntity entity);

        IQueryable<TEntity> Get();

        bool Save();
    }
}
