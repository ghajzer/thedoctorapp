﻿using System;
using System.Linq;
using DataAccess;
using Repository.Generic.Interface;

namespace Repository.Generic
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApiContext context;

        protected Repository(ApiContext context)
        {
            this.context = context;
        }

        public abstract TEntity Get(int id);

        public bool Add(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            return Save();
        }

        public bool Delete(int id)
        {
            var entity = Get(id);
            if (entity == null)
            {
                throw new ArgumentException($"No resource with id {id}");
            }
            return Delete(entity);
        }

        public bool Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            return Save();
        }

        public IQueryable<TEntity> Get()
        {
            return context.Set<TEntity>();
        }

        public bool Save()
        {
            return context.SaveChanges() > 0;
        }
    }
}
