﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logic.Dto.Patient;
using Newtonsoft.Json;

namespace WpfDoctorClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RefreshPatients();
        }

        private void RemoveClick(object sender, RoutedEventArgs e)
        {
            var selectedPatient = (PatientDto)Patients.SelectedItem;
            if (selectedPatient == null)
            {
                MessageBox.Show("Nincs kiválasztva beteg!");
                return;
            }
            using (var client = new HttpClient())
            {
                try
                {
                    var result = client.DeleteAsync($"https://localhost:44319/api/Patients/{selectedPatient.Id}").Result;
                    MessageBox.Show(result.IsSuccessStatusCode
                        ? "Beteg törölve!"
                        : "Hiba a beteg törlése közben!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Hiba a beteg törlése közben!\nA kapcsolat megszakadt a szerverrel!");
                }
            }
            RefreshPatients();
        }

        private IEnumerable<PatientDto> GetPatients()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var result = client.GetAsync("https://localhost:44319/api/Patients").Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        return null;
                    }

                    var content = result.Content.ReadAsStringAsync().Result;
                    var patients = JsonConvert.DeserializeObject<IEnumerable<PatientDto>>(content);

                    return patients;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        private void RefreshPatients()
        {
            var patients = GetPatients();
            if (patients != null)
            {
                Patients.ItemsSource = patients;
            }
            else
            {
                MessageBox.Show("Hiba a betegek betöltése közben!");
            }
            
        }

        private void Patients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedPatient = (PatientDto) Patients.SelectedItem;
            if (selectedPatient != null)
            {
                Name.Text = selectedPatient.Name;
                Address.Text = selectedPatient.Address;
                TajNumber.Text = selectedPatient.TajNumber.ToString();
                Complaint.Document.Blocks.Clear();
                Complaint.Document.Blocks.Add(new Paragraph(new Run(selectedPatient.Complaint)));
            }
           
        }

        private void RefreshClick(object sender, RoutedEventArgs e)
        {
            RefreshPatients();
        }
    }
}
