﻿namespace DataAccess
{
    public class Patient : BaseEntity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public long TajNumber { get; set; }

        public string Complaint { get; set; }
    }
}
