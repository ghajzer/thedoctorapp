﻿using System;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class ApiContext : DbContext
    {
        public DbSet<Patient> Patients { get; set; }

        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
        }
    }
}
